<?php get_header(); ?> <section class="banner-engenharia banner"><div class="container h-100"><div class="full-center h-100"><div class="col-12 d-flex justify-content-center"><img class="img-fluid size-img mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-engenharia.png" alt="Engenharia"><h1 class="title-page"><?php wp_title(""); ?></h1></div></div></div><div class="mini-bar py-2 bg-lgrey"><div class="container"><div class="breadcrumbs col-12">Home / <?php wp_title(""); ?></div></div></div></section><section class="py-5"><div class="container"><div class="text-center py-5">Com o propósito de entregar soluções assertivas e efetivas, que estejam em total<br>conformidade com os objetivos do cliente, os serviços de Engenharia da <strong>ATIVA TS</strong><br>oferecem garantia de entrega, tanto técnica quanto de execução, e se propõem a<br>atender os prazos, e a reduzir custos.</div><div class="row py-4 col-12 m-auto text-center"><p class="btn-b m-auto">Conheça a ampla gama de serviços de engenharia<br>que a ATIVA TS coloca à disposição da sua empresa:</p></div></div></section><section class="py-5 bg-lgrey servicos-engenharia"><div class="container"><div class="row"> <?php

      $paged = get_query_var('paged') ? get_query_var('paged') : 1;

      $args = array(

        'post_type' => 'engenharia',

        'order' => 'ASC',

        'posts_per_page' => '-1',

        'paged' => $paged,

      );

      $loop = new wp_query($args);

      while ($loop->have_posts()) : $loop->the_post() ?> <a href="<?= the_permalink() ?>" class="col-md-6 p-0 d-md-flex"><div class="col-md-6 py-4 text-center"><img class="mr-3" src="<?= the_field('thumb') ?>"></div><div class="col-md-6 py-4"><h5 class="color-blue underline-blue"><?= the_title(); ?></h5><div class="block"> <?= the_field('descricao_curta') ?> </div></div></a> <?php endwhile; ?> </div></div></section><section class="py-5"> <?php include "conheca.php" ?> </section> <?php get_footer(); ?>