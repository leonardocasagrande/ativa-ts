$(".owl-default").owlCarousel({
  loop: false,
  margin: 50,
  nav: true,
  navText: [
    "<i class='fas chev fa-chevron-left'></i> ",
    "<i class='fas chev fa-chevron-right'></i>"
  ],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
});

var SPMaskBehavior = function(val) {
    return val.replace(/\D/g, "").length === 11
      ? "(00) 00000-0000"
      : "(00) 0000-00009";
  },
  spOptions = {
    onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
  };

$(".cellphone").mask(SPMaskBehavior, spOptions);
