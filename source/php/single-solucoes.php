<?php get_header(); ?>

<div class="bg-single py-5" style="background: url(<?= the_field('imagem') ?>);">

  <div class="container text-center">

    <h1 class="text-white title-page-white my-5 py-3"><?= the_title(); ?></h1>

  </div>

</div>

<section>

  <div class="mini-bar py-2 bg-lgrey">

    <div class="container">

      <div class="breadcrumbs col-12"><a href="/">Home</a> / <a href="solucoes">Soluções</a> / <a href="<?php get_the_permalink(); ?>"><?= the_title(); ?></a></div>
    </div>

  </div>

</section>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="container py-5">

      <?= the_content() ?>

    </div>

<?php endwhile;

else : endif; ?>

<section class="py-5">

  <?php include "conheca.php" ?>

</section>

<?php get_footer(); ?>