<?php get_header(); ?>

<section class="banner-manutencao banner">
  <div class="container h-100">
    <div class="full-center h-100">
      <div class="col-12 d-flex justify-content-center">
        <h1 class="title-page text-center">Atendimento de Manutenção de HW dos
          desktops e Notebooks dos usuários IBM</h1>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="mini-bar py-2 bg-lgrey">
    <div class="container">
      <div class="breadcrumbs col-12">Home / Engenharia / Atendimento de Manutenção</div>
    </div>
  </div>
</section>

<section>
  <div class="container py-5">
    <div class="col-md-10 m-auto py-3">
      <p>In gravida sem eget tristique sagittis. Mauris mattis, arcu ut tempus sagittis, leo nibh gravida augue, in dictum arcu nunc scelerisque leo. Vestibulum sed facilisis velit. Curabitur porta, augue non aliquet porta, libero mi feugiat augue, in volutpat diam tellus vel est. Pellentesque ut nibh id turpis finibus aliquam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis ipsum nisi, scelerisque sed nulla ac, vulputate congue leo.</p>
    </div>
    <img class="img-fluid" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/atendimento-manutenção.png" alt="Atendimento em Manutenção">

    <div class="col-md-10 m-auto py-4">
      <p class="py-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ut quam nibh. Morbi aliquet quam ac risus molestie fermentum. Donec id mi eu nisl semper faucibus. Aliquam accumsan ipsum vitae ipsum laoreet sollicitudin id eu nunc. Nulla at lorem id diam pretium porta vitae sit amet turpis. Praesent cursus quis massa tincidunt tristique. Nam commodo dignissim ligula a finibus. Proin quis pellentesque odio, at suscipit lorem. Fusce laoreet dui id augue faucibus, in scelerisque risus luctus. Donec at magna pulvinar, aliquet turpis sit amet, sodales nibh. Donec consectetur arcu nec consequat varius. Quisque ac ex nec leo tincidunt malesuada consectetur sit amet ante. Nam eget dapibus nibh. Maecenas ultricies tellus leo, vitae pulvinar tortor egestas nec.</p>
      <br>
      <p>Donec quis tincidunt ligula, vel congue nisl. Quisque dui neque, vestibulum ut sapien vitae, iaculis mattis lacus. Quisque facilisis finibus cursus. Suspendisse et gravida lacus. Proin vel nisl sit amet neque blandit tincidunt nec id turpis. Vestibulum at ipsum leo. Sed eget enim non neque viverra fringilla vel vestibulum nunc. Nunc auctor sed arcu in pretium. Pellentesque vitae consequat justo. Nulla facilisi. Morbi tempus consectetur diam non fringilla. Donec tempor scelerisque tempor. Praesent vel mattis enim. Suspendisse potenti. Sed sodales pulvinar luctus. Etiam gravida dui nec condimentum ultricies.</p>
      <br>
      <p>Curabitur facilisis nisl et risus tempor, ut consectetur libero ultricies. Quisque est erat, pretium et eros vitae, aliquet accumsan felis. Donec ullamcorper, metus id hendrerit fermentum, erat lectus congue augue, a iaculis ex quam ut felis. Proin interdum id nisl et dapibus. Nulla lacinia erat ut dui sollicitudin, quis sollicitudin neque congue. Morbi quis velit mi. Maecenas sodales purus id neque vulputate mattis.</p>
      <br>
      <p>Nunc venenatis mauris in quam aliquam, vitae tempus libero ullamcorper. Maecenas placerat ligula et hendrerit ultrices. Praesent gravida aliquet lorem, non pulvinar diam dapibus nec. Suspendisse a mattis mauris. Cras blandit dui vel mauris efficitur, vel aliquam est lobortis. Sed pharetra ex orci, et vestibulum nulla molestie in. Mauris dignissim sagittis hendrerit. Suspendisse id felis rhoncus, vehicula ligula non, tincidunt lorem. Aenean ac leo pretium, cursus augue eu, pretium ante. Duis ut tristique arcu. Ut nec pellentesque quam, feugiat auctor enim. Ut at viverra nibh. Ut condimentum lectus ac posuere tempor.</p>
    </div>
    <div class="col-12 d-flex justify-content-center paragraph-page">
      <a class="text-uppercase btn-b" href="contato">Entre em contato</a>
    </div>

  </div>
</section>

<?php get_footer(); ?>