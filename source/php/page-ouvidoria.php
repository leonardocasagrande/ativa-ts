<?php get_header(); ?>

<section class="banner-ouvidoria banner">
  <div class="container h-100">
    <div class="full-center h-100">
      <div class="col-12 d-flex justify-content-center">
        <img class="img-fluid size-img mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/ouvidoria.png" alt="Imagem Ouvidoria">
        <h1 class="title-page-white">Ouvidoria</h1>
      </div>
    </div>
  </div>
</section>

<section class="section-form">
  <div class="container py-5">
    <div class="col-md-8">
      <p>Este é um canal exclusivo para eventuais denúncias ou perguntas relacionadas à conduta ética sobre associados ou seus parceiros de negócio. Outros problemas não relacionados a este tema devem ser direcionados diretamente às empresas.</p>
      <p class="color-paragraph">A <strong>ATIVA TS</strong> dispõe de um canal de Ouvidoria preparado para receber, analisar e solucionar questões relacionadas ao Código de Conduta sobre associados ou seus parceiros de negócio. Esse canal pode ser acessado pelos públicos interno e externo.</p>
    </div>

    <?php include 'form.php' ?>

  </div>
</section>

<?php get_footer(); ?>