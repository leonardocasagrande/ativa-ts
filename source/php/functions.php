<?php

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

(add_theme_support('post-thumbnails'));


function replace_core_jquery_version()
{
    wp_deregister_script('jquery');
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", array(), '3.4.1');
}
add_action('wp_enqueue_scripts', 'replace_core_jquery_version');



function create_post_type()
{


    /**
     * solucoes
     */
    register_post_type(
        'solucoes',
        array(
            'labels' => array(
                'name' => __('Soluções'),
                'singular_name' => __('Soluções')
            ),
            'public' => true,
            'has_archive' => false,


        )
    );

    /**
     * engenharia
     */
    register_post_type(
        'engenharia',
        array(
            'labels' => array(
                'name' => __('Engenharia'),
                'singular_name' => __('Engenharia')
            ),
            'public' => true,
            'has_archive' => false,

        )
    );
}

add_action('init', 'create_post_type');
?>