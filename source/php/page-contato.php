<?php get_header(); ?>

<section class="banner-trabalhe banner">
  <div class="container h-100">
    <div class="full-center h-100">
      <div class="col-12 d-flex justify-content-center">
        <img class="img-fluid size-img mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/trabalhe-conosco.png" alt="Trabalhe">
        <h1 class="title-page">Contato</h1>
      </div>
    </div>
  </div>
</section>

<section class="section-form">
  <div class="container py-5">
    <div class="row">
      <div class="col-md-6">
        <p>Oferecemos suporte de acordo com as necessidades de nossos clientes. Para qualquer informação, dúvida ou orçamento, entre em contato conosco.</p>
        <p class="color-paragraph text-uppercase">Preencha o formulário, envie sua mensagem e nossa equipe entrará em contato com você:</p>
      </div>

      <div class="col-md-6">
        <?php echo do_shortcode('[contact-form-7 id="110" title="Form Contato"]') ?>
      </div>
    </div>

  </div>
</section>


<?php get_footer(); ?>