<?php get_header(); ?>

<section class="banner-sobrenos banner">
  <div class="container h-100">
    <div class="full-center h-100">
      <div class="col-12 d-flex justify-content-center">
        <img class="img-fluid size-img mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobre-nos.png" alt="Imagem Sobre Nós">
        <h1 class="title-page">Sobre<br> Nós</h1>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container py-5">
    <div class="row py-5">
      <div class="col-md-6">
        <p>Entregar soluções em Tecnologia da Informação aplicadas a diferentes necessidades, transformando estratégias em resultados positivos. Esse é o objetivo da ATIVA TS desde 1998, quando foi fundada. Através de uma sólida experiência somada a um comportamento empresarial estratégico, fornece soluções de alto padrão de qualidade.</p>
        <br>
        <p>Seja por meio da tecnologia e seus benefícios ou da aplicação de seus conhecimentos, a ATIVA TS acredita na missão de transformar positivamente a vida das pessoas. Toda a inteligência e dedicação de sua equipe altamente capacitada são concentradas no desenvolvimento das melhores e mais adequadas soluções para seus clientes.</p>
      </div>
      <div class="col-md-6">
        <p>A ATIVA TS é referência em serviços de consultoria e tecnologia no mercado brasileiro. Com matriz localizada em Santo André – SP, possui uma estrutura formada por 8 filiais distribuídas pelos estados de São Paulo, Mato Grosso e Mato Grosso do Sul. Além disso, conta com equipe e parceiros nas principais capitais do Brasil focados na qualidade dos atendimentos, o que fortalece sua presença em todo território nacional.</p>
        <br>
        <p><strong>Certificada pela Dun & Bradstreet International Ltda. (D&B), líder mundial em fornecimento de informações para áreas de crédito, base de dados de marketing, análise de compras B2B e áreas de suporte a serviços, a ATIVA TS é preparada para proporcionar segurança, eficiência e inovação em TI.</strong></p>
      </div>
    </div>
  </div>
</section>

<section class="section-business py-5">
  <div class="container bg-whiteilac">
    <div class="row align-items-center">
      <div class="col-md-6 mt-sm-n5">
        <img class="img-fluid" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/company-mission.png" alt="Missão da Empresa">
      </div>
      <div class="col-md-6">
        <h3 class="text-uppercase title-business py-3 text-md-left text-center">Missão da empresa</h3>
        <p>Temos como missão solidificar cada vez mais nossas parcerias, acentuando o compromisso de fidelidade com os clientes, fornecendo serviços de tecnologia da informação através de nossos talentos humanos, com elevado padrão de qualidade, eficiência, pontualidade e excelência no atendimento e execução os serviços.</p>
      </div>
      <div class="col-12 py-4">
        <h3 class="title-business text-uppercase text-md-left text-center">Política da qualidade</h3>
        <p class="py-3">A ATIVA TS tem como Política do Sistema de Gestão Integrado oferecer as melhores soluções em informática com responsabilidade socioambiental, melhorando continuamente seus processos e a cadeia de fornecimento, buscando:</p>
        <p>
          a eliminação ou redução dos riscos à saúde e segurança;<br>
          a prevenção da poluição e a proteção ao meio ambiente;<br>
          o atendimento às legislações aplicáveis e aos requisitos de clientes;<br>
          valorizar os princípios éticos nas relações com clientes, fornecedores, associados e comunidade;<br>
          cabe a todos os profissionais disseminá-la e incorporá-la em todas as suas atitudes, posicionamentos, atividades e resultados da empresa de forma regular e contínua.
        </p>
      </div>
    </div>
  </div>
</section>

<section class="section-know">
  <div class="container py-5">
    <h3 class="text-center py-3 title-know">Conheça também os serviços da Ativa TS</h3>
    <div class="row">
      <div class="col-md-6 d-flex justify-content-center">
        <a class="text-uppercase btn btn-outline-know btn-block mt-3" href="<?= get_site_url(); ?>/engenharia/">Engenharia</a>
      </div>
      <div class="col-md-6 d-flex justify-content-center">
        <a class="text-uppercase btn btn-outline-know-ti btn-block mt-3" href="<?= get_site_url(); ?>/servicos-de-ti/">Serviços de TI</a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>