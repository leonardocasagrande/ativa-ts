<?php get_header(); ?>

<div class="single">

  <div class="container">

    <h1 class="text-center color-green"><?= the_title(); ?></h1>

  </div>

</div>

<?php get_footer(); ?>