<?php get_header(); ?>

<section class="banner-infraestrutura banner">
  <div class="container h-100">
    <div class="full-center h-100">
      <div class="col-12 d-flex justify-content-center">
        <h1 class="title-page-white">Infraestrutura</h1>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="mini-bar py-2 bg-lgrey">
    <div class="container">
      <div class="breadcrumbs col-12">Home / Engenharia / Infraestrutura</div>
    </div>
  </div>
</section>

<section>
  <div class="container py-4">
    <img class="img-fluid py-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/infraestrutura.jpg" alt="Infraestrutura">

    <div class="col-md-10 m-auto py-3">
      <p>Desde as instalações de eletrodutos até as instalações de FibberRunner são realizadas pela ATIVA TS, com atenção em cada detalhe para que sua infraestrutura mantenha seu perfeito funcionamento.</p>
    </div>
  </div>
</section>


<section>
  <div class="container">

    <div class="col-md-12 py-3 text-center">
      <h4 class="color-blue">
        <span class="span-subtitle text-uppercase">Veja o que a ATIVA TS pode fazer para a sua infraestrutura:</span></h4>
    </div>

    <div class="row py-4">
      <div class="col-md-6 d-flex justify-content-center">
        <ul class="text-uppercase">
          <li>Instalação de Esteira/Leito</li>
          <li>Instalação de Fibber Runner</li>
          <li>Instalação de Eletrodutos/Eletrocalhas</li>
        </ul>
      </div>

      <div class="col-md-6 d-flex justify-content-center">
        <ul class="text-uppercase">
          <li>Instalação de Infraestrutura Subterrânea</li>
          <li>Instalação de Infraestrutura em Aço Inox</li>
        </ul>
      </div>
    </div>
    <div class="col-12 d-flex justify-content-center py-4">
      <a class="btn-b text-uppercase" href="contato">Entre em contato</a>
    </div>
  </div>
</section>

<section class="py-3">
  <?php include "conheca.php" ?>
</section>


<?php get_footer(); ?>